#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include "battlefield.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionInit_battle_triggered();

    void on_pushButton_clicked();

    void on_actionLoad_file_triggered();

private:
    Ui::MainWindow *ui;
    Battlefield * Battle;
};

#endif // MAINWINDOW_H
