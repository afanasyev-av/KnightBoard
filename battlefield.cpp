#include "battlefield.h"

Battlefield::Battlefield()
{
    battlefield = nullptr;
}

Battlefield::Battlefield(int width,int height)
{
    Width = width;
    Height = height;
    int i;
    battlefield = new int*[Width];
    for(i=0;i<Width;i++){
        battlefield[i] = new int[Height];
    }
    int j;

    for(i=0;i<Width;i++){
        for(j=0;j<Height;j++){
            battlefield[i][j] = 0;
        }
    }

    fieldmask = new char*[Width];
    for(i=0;i<Width;i++){
        fieldmask[i] = new char[Height];
    }

    for(i=0;i<Width;i++){
        for(j=0;j<Height;j++){
            fieldmask[i][j] = '.';
        }
    }
}

Battlefield::~Battlefield()
{
    if(battlefield != nullptr){
        int i;
        for(i=0;i<Width;i++){
            delete[] battlefield[i];
        }
        delete[] battlefield;
    }
}

void Battlefield::setStartPoint(int sx,int sy)
{
    start.x = sx;
    start.y = sy;
}

void Battlefield::setEndPoint(int ex,int ey)
{
    end.x = ex;
    end.y = ey;
}

int Battlefield::getWidth(void)
{
    return Width;
}

int Battlefield::getHeight(void)
{
    return Height;
}

char Battlefield::getFieldMask(int x,int y)
{
    if((x>=0)&&(x<Width)&&(y>=0)&&(y<Height)){
        return fieldmask[x][y];
    }
}

void Battlefield::getWay(void)
{
    //vawes method
    int x,y;
    int val;
    queue<point> selectpoint;
    selectpoint.push(start);
    battlefield[start.x][start.y] = 1;
    point cur;
    int i=0;
    while(!selectpoint.empty()){
        cur = selectpoint.front();
        selectpoint.pop();
        x = cur.x;
        y = cur.y;
        val = battlefield[x][y];
        bool fl;
        if((x-2 >= 0)&&(y-1 >= 0)&&(battlefield[x-2][y-1] == 0)){
            fl = true;
            if(fieldmask[x-2][y-1]=='R'){
                fl = false;
            }
            if((fieldmask[x-2][y-1]=='B')||(fieldmask[x-1][y]=='B')||(fieldmask[x-1][y-1]=='B')){
                fl = false;
            }
            if(fieldmask[x-2][y-1]=='T'){
                cur = teleport(x-2,y-1);
                battlefield[cur.x][cur.y]=val+1;
                selectpoint.push(cur);
                fl = false;
            }
            if(fl){
                battlefield[x-2][y-1]=val+1;
                cur.x = x-2;
                cur.y = y-1;
                selectpoint.push(cur);
            }
        }
        if((x-2 >= 0)&&(y+1 < Height)&&(battlefield[x-2][y+1] == 0)){
            fl = true;
            if(fieldmask[x-2][y+1]=='R'){
                fl = false;
            }
            if((fieldmask[x-2][y+1]=='B')||(fieldmask[x-1][y]=='B')||(fieldmask[x-1][y+1]=='B')){
                fl = false;
            }
            if(fieldmask[x-2][y+1]=='T'){
                cur = teleport(x-2,y+1);
                battlefield[cur.x][cur.y]=val+1;
                selectpoint.push(cur);
                fl = false;
            }
            if(fl){
                battlefield[x-2][y+1]=val+1;
                cur.x = x-2;
                cur.y = y+1;
                selectpoint.push(cur);
            }
        }
        if((x+2 < Width)&&(y-1 >= 0)&&(battlefield[x+2][y-1] == 0)){
            fl = true;
            if(fieldmask[x+2][y-1]=='R'){
                fl = false;
            }
            if((fieldmask[x+2][y-1]=='B')||(fieldmask[x+1][y]=='B')||(fieldmask[x+1][y-1]=='B')){
                fl = false;
            }
            if(fieldmask[x+2][y-1]=='T'){
                cur = teleport(x+2,y-1);
                battlefield[cur.x][cur.y]=val+1;
                selectpoint.push(cur);
                fl = false;
            }
            if(fl){
                battlefield[x+2][y-1]=val+1;
                cur.x = x+2;
                cur.y = y-1;
                selectpoint.push(cur);
            }
        }
        if((x+2 < Width)&&(y+1 < Height)&&(battlefield[x+2][y+1] == 0)){
            fl = true;
            if(fieldmask[x+2][y+1]=='R'){
                fl = false;
            }
            if((fieldmask[x+2][y+1]=='B')||(fieldmask[x+1][y]=='B')||(fieldmask[x+1][y+1]=='B')){
                fl = false;
            }
            if(fieldmask[x+2][y+1]=='T'){
                cur = teleport(x+2,y+1);
                battlefield[cur.x][cur.y]=val+1;
                selectpoint.push(cur);
                fl = false;
            }
            if(fl){
                battlefield[x+2][y+1]=val+1;
                cur.x = x+2;
                cur.y = y+1;
                selectpoint.push(cur);
            }
        }

        if((x-1 >= 0)&&(y-2 >= 0)&&(battlefield[x-1][y-2] == 0)){
            fl = true;
            if(fieldmask[x-1][y-2]=='R'){
                fl = false;
            }
            if((fieldmask[x-1][y-2]=='B')||(fieldmask[x-1][y-1]=='B')||(fieldmask[x][y-1]=='B')){
                fl = false;
            }
            if(fieldmask[x-1][y-2]=='T'){
                cur = teleport(x-1,y-2);
                battlefield[cur.x][cur.y]=val+1;
                selectpoint.push(cur);
                fl = false;
            }
            if(fl){
                battlefield[x-1][y-2]=val+1;
                cur.x = x-1;
                cur.y = y-2;
                selectpoint.push(cur);
            }
        }
        if((x+1 < Width)&&(y-2 >= 0)&&(battlefield[x+1][y-2] == 0)){
            fl = true;
            if(fieldmask[x+1][y-2]=='R'){
                fl = false;
            }
            if((fieldmask[x+1][y-2]=='B')||(fieldmask[x+1][y-1]=='B')||(fieldmask[x][y-1]=='B')){
                fl = false;
            }
            if(fieldmask[x+1][y-2]=='T'){
                cur = teleport(x+1,y-2);
                battlefield[cur.x][cur.y]=val+1;
                selectpoint.push(cur);
                fl = false;
            }
            if(fl){
                battlefield[x+1][y-2]=val+1;
                cur.x = x+1;
                cur.y = y-2;
                selectpoint.push(cur);
            }
        }
        if((x-1 >= 0)&&(y+2 < Height)&&(battlefield[x-1][y+2] == 0)){
            fl = true;
            if(fieldmask[x-1][y+2]=='R'){
                fl = false;
            }
            if((fieldmask[x-1][y+2]=='B')||(fieldmask[x-1][y+1]=='B')||(fieldmask[x][y+1]=='B')){
                fl = false;
            }
            if(fieldmask[x-1][y+2]=='T'){
                cur = teleport(x-1,y+2);
                battlefield[cur.x][cur.y]=val+1;
                selectpoint.push(cur);
                fl = false;
            }
            if(fl){
                battlefield[x-1][y+2]=val+1;
                cur.x = x-1;
                cur.y = y+2;
                selectpoint.push(cur);
            }
        }
        if((x+1 < Width)&&(y+2 < Height)&&(battlefield[x+1][y+2] == 0)){
            fl = true;
            if(fieldmask[x+1][y+2]=='R'){
                fl = false;
            }
            if((fieldmask[x+1][y+2]=='B')||(fieldmask[x+1][y+1]=='B')||(fieldmask[x][y+1]=='B')){
                fl = false;
            }
            if(fieldmask[x+1][y+2]=='T'){
                cur = teleport(x+1,y+2);
                battlefield[cur.x][cur.y]=val+1;
                selectpoint.push(cur);
                fl = false;
            }
            if(fl){
                battlefield[x+1][y+2]=val+1;
                cur.x = x+1;
                cur.y = y+2;
                selectpoint.push(cur);
            }
        }
        if(battlefield[end.x][end.y]>0)break;
        i++;
        if(i>=Width*Height)break;
    }

    val = battlefield[end.x][end.y];
    if(val>0){
        cur = end;
        way.push_back(cur);
        while((cur.x!=start.x)&&(cur.y!=start.y)){
            cur = nextstep(cur,val);
            val = battlefield[cur.x][cur.y];
            if(fieldmask[cur.x][cur.y]=='.'){
                way.push_back(cur);
                continue;
            }
            if(fieldmask[cur.x][cur.y]=='W'){
                way.push_back(cur);
                way.push_back(cur);
                continue;
            }
            if(fieldmask[cur.x][cur.y]=='L'){
                way.push_back(cur);
                way.push_back(cur);
                way.push_back(cur);
                way.push_back(cur);
                way.push_back(cur);
                continue;
            }
            if(fieldmask[cur.x][cur.y]=='T'){
                way.push_back(cur);
                cur = teleport(cur.x,cur.y);
                way.push_back(cur);
                continue;
            }
        }
    }
}

point Battlefield::nextstep(point cur,int val)
{
    int x,y;
    x = cur.x;
    y = cur.y;

    vector <point> nextpoints;

    if((x-2 >= 0)&&(y-1 >= 0)&&(battlefield[x-2][y-1] == val-1)){
        cur.x = x-2;
        cur.y = y-1;
        nextpoints.push_back(cur);
        //return cur;
    }
    if((x-2 >= 0)&&(y+1 < Height)&&(battlefield[x-2][y+1] == val-1)){
        cur.x = x-2;
        cur.y = y+1;
        nextpoints.push_back(cur);
        //return cur;
    }
    if((x+2 < Width)&&(y-1 >= 0)&&(battlefield[x+2][y-1] == val-1)){
        cur.x = x+2;
        cur.y = y-1;
        nextpoints.push_back(cur);
        //return cur;
    }
    if((x+2 < Width)&&(y+1 < Height)&&(battlefield[x+2][y+1] == val-1)){
        cur.x = x+2;
        cur.y = y+1;
        nextpoints.push_back(cur);
        //return cur;
    }

    if((x-1 >= 0)&&(y-2 >= 0)&&(battlefield[x-1][y-2] == val-1)){
        cur.x = x-1;
        cur.y = y-2;
        nextpoints.push_back(cur);
        //return cur;
    }
    if((x+1 < Width)&&(y-2 >= 0)&&(battlefield[x+1][y-2] == val-1)){
        cur.x = x+1;
        cur.y = y-2;
        nextpoints.push_back(cur);
        //return cur;
    }
    if((x-1 >= 0)&&(y+2 < Height)&&(battlefield[x-1][y+2] == val-1)){
        cur.x = x-1;
        cur.y = y+2;
        nextpoints.push_back(cur);
        //return cur;
    }
    if((x+1 < Width)&&(y+2 < Height)&&(battlefield[x+1][y+2] == val-1)){
        cur.x = x+1;
        cur.y = y+2;
        nextpoints.push_back(cur);
        //return cur;
    }
    if(val == 3){
        val = 3;
    }
    for(auto i=0;i<nextpoints.size();i++){
        if(fieldmask[nextpoints[i].x][nextpoints[i].y]=='.'){
            return nextpoints[i];
        }
    }
    for(auto i=0;i<nextpoints.size();i++){
        if(fieldmask[nextpoints[i].x][nextpoints[i].y]=='W'){
            return nextpoints[i];
        }
    }
    return nextpoints[0];
}

void Battlefield::loadFile(string filename)
{
    string buf;
    ifstream fin(filename);
    fin >> buf;
    int W = buf.length();
    vector<string> filestring;
    filestring.push_back(buf);
    while (!fin.eof()) {
        buf.clear();
        fin >> buf;
        //очему-то иногда четает еще 1 строку
        if(!buf.empty()){
            filestring.push_back(buf);
        }
    }
    int H = filestring.size();
    fin.close();

    int i;
    fieldmask = new char*[W];
    for(i=0;i<W;i++){
        fieldmask[i] = new char[H];
    }
    int j;

    for(i=0;i<W;i++){
        for(j=0;j<H;j++){
            buf = filestring[j];
            fieldmask[i][j] = buf[i];
        }
    }
    filestring.clear();

    Width = W;
    Height = H;
    //int i;
    battlefield = new int*[Width];
    for(i=0;i<Width;i++){
        battlefield[i] = new int[Height];
    }
    //int j;

    for(i=0;i<Width;i++){
        for(j=0;j<Height;j++){
            battlefield[i][j] = 0;
        }
    }
}

point Battlefield::teleport(int x, int y)
{
    point cur;
    int i,j;
    for(i=0;i<Width;i++){
        for(j=0;j<Height;j++){
            if((fieldmask[i][j] == 'T')&&(i!=x)&&(j!=y)){
                cur.x = i;
                cur.y = j;
                return cur;
            }
        }
    }
}
