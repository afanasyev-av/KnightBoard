#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionInit_battle_triggered()
{
    Battle= new Battlefield(8,8);
    ui->tableWidget->setColumnCount(8);
    ui->tableWidget->setRowCount(8);

    QTableWidgetItem *ptwi;
    int i,j;
    for(i=0;i<Battle->getWidth();i++){
        for(j=0;j<Battle->getHeight();j++){
            ptwi = new QTableWidgetItem(QString("%1").arg(Battle->getFieldMask(i,j)));
            ui->tableWidget->setItem(j,i,ptwi);
        }
    }
}

void MainWindow::on_pushButton_clicked()
{
    int Xs,Ys;
    int Xe,Ye;
    Xs = ui->XsEdit->text().toInt()-1;
    Ys = ui->YsEdit->text().toInt()-1;
    Xe = ui->XeEdit->text().toInt()-1;
    Ye = ui->YeEdit->text().toInt()-1;
    Battle->setStartPoint(Xs,Ys);
    Battle->setEndPoint(Xe,Ye);

    QTableWidgetItem *ptwi;
    ptwi = new QTableWidgetItem("S");
    ui->tableWidget->setItem(Ys,Xs,ptwi);
    ptwi = new QTableWidgetItem("E");
    ui->tableWidget->setItem(Ye,Xe,ptwi);

    Battle->getWay();
    int x,y;
    /*
    int i,j;
    for(i=0;i<Battle->getWidth();i++){
        for(j=0;j<Battle->getHeight();j++){
            ptwi = new QTableWidgetItem(QString("%1").arg(Battle->battlefield[i][j]));
            ui->tableWidget->setItem(j,i,ptwi);
        }
    }
    */
    for(auto it=Battle->way.begin();it!=Battle->way.end();it++){
        x = (*it).x;
        y = (*it).y;
        QString str;
        str = ui->tableWidget->takeItem(y,x)->text();
        ptwi = new QTableWidgetItem(str+"K");

        ui->tableWidget->setItem(y,x,ptwi);
    }

}

void MainWindow::on_actionLoad_file_triggered()
{
    QString FileName = QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("file data (*.*)"));
    if(FileName.isEmpty())return;
    Battle= new Battlefield;
    Battle->loadFile(FileName.toStdString());
    ui->tableWidget->setColumnCount(Battle->getWidth());
    ui->tableWidget->setRowCount(Battle->getHeight());

    QTableWidgetItem *ptwi;
    int i,j;
    for(i=0;i<Battle->getWidth();i++){
        for(j=0;j<Battle->getHeight();j++){
            ptwi = new QTableWidgetItem(QString("%1").arg(Battle->getFieldMask(i,j)));
            ui->tableWidget->setItem(j,i,ptwi);
        }
    }
}
