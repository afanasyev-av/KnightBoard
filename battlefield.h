#ifndef BATTLEFIELD_H
#define BATTLEFIELD_H

#include <queue>
#include <vector>
#include <fstream>
#include <string>

using namespace std;

struct point
{
    int x;
    int y;
};

class Battlefield
{
    public:
        Battlefield();
        Battlefield(int width,int height);
        ~Battlefield();

        void setStartPoint(int sx,int sy);
        void setEndPoint(int ex,int ey);

        void getWay(void);
        vector <point> way;
        void loadFile(string filename);

        int getWidth(void);
        int getHeight(void);
        char getFieldMask(int x,int y);

    private:
        int Width;
        int Height;
        int **battlefield;
        point start;
        point end;
        char **fieldmask;

        point nextstep(point cur,int val);
        point teleport(int x,int y);
};

#endif // BATTLEFIELD_H
