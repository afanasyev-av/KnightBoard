#-------------------------------------------------
#
# Project created by QtCreator 2016-10-07T17:37:04
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = knight
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    battlefield.cpp

HEADERS  += mainwindow.h \
    battlefield.h

FORMS    += mainwindow.ui

CONFIG += c++11
